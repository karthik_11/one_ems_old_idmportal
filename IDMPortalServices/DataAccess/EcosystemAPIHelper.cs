﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataObjects;
using MongoDB.Bson;
using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Configuration;
using System.Net;


namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public static class EcosystemAPIHelper
    {
        private static string baseAddress = ConfigurationManager.AppSettings["EcoSystemBaseUri"];

        public static HttpResponseMessage GetEcosystemPackages()
        {
            HttpResponseMessage response;
            try
            {
                using (var client = new HttpClient())
                {
                    if (!baseAddress.EndsWith(@"/"))
                    {
                        baseAddress += @"/";
                    }
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // HTTP GET
                    //response = client.GetAsync("pma/subscriptions").Result;
                    response = client.GetAsync("api/product").Result;
                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage() { 
                    StatusCode = HttpStatusCode.InternalServerError, 
                    Content = new StringContent(ex.Message) };
            }
            return response;
        }

        public static HttpResponseMessage GetEcosystemInstallBasePackages()
        {
            HttpResponseMessage response;
            try
            {
                using (var client = new HttpClient())
                {
                    if (!baseAddress.EndsWith(@"/"))
                    {
                        baseAddress += @"/";
                    }
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // HTTP GET
                    response = client.GetAsync("pma/field/components").Result;
                   
                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };
            }
            return response;
        }

        //public static int PostSWDRequest(SWDRequestMsg swdRequest)
        public static HttpResponseMessage PostSWDeploymentRequest(string swDeploymentReq)
        {
            HttpResponseMessage response; 
            using (var client = new HttpClient())
            {
                if (!baseAddress.EndsWith(@"/"))
                {
                    baseAddress += @"/";
                }
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));

                string serviceUrl = "pma/in/softwaredeployment/manifest";
                response = client.PostAsJsonAsync(serviceUrl, swDeploymentReq).Result;
                return response;                
            }            
        }

        //public static int PostSWDRequest(SWDRequestMsg swdRequest)
        public static HttpResponseMessage PostSWDRequest(SWDJsonPost swdRequest)
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                if (!baseAddress.EndsWith(@"/"))
                {
                    baseAddress += @"/";
                }
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string serviceUrl = "pma/in/softwaredistribution/subscriptions";
                response = client.PostAsJsonAsync(serviceUrl, JObject.Parse(JsonConvert.SerializeObject(swdRequest))).Result;
                return response;
            }
        }
         
        /// <summary>
        /// Post Site Exception Request to IDM Ecosystem
        /// </summary>
        /// <param name="siteExpReq"></param>
        /// <returns></returns>
        public static HttpResponseMessage PostSiteExceptionRequest(string siteExpReq)
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                if (!baseAddress.EndsWith(@"/"))
                {
                    baseAddress += @"/";
                }
                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));

                string serviceUrl = "pma/in/descriptor/site";
                response = client.PostAsJsonAsync(serviceUrl, siteExpReq).Result;
                return response;
            }
        }

        /// <summary>
        /// Gets Ecosystem Deployment Packages
        /// </summary>
        /// <param name="sitekey"></param>
        /// <returns>HttpResponseMessage</returns>
        public static HttpResponseMessage GetEcosystemDeploymentPackages(string sitekey)
        {
            HttpResponseMessage response;
            try
            {
                using (var client = new HttpClient())
                {
                    if (!baseAddress.EndsWith(@"/"))
                    {
                        baseAddress += @"/";
                    }
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // HTTP GET
                    string serviceUrl = "pma/facts/" + sitekey + "/modules/PCM";
                    response = client.GetAsync(serviceUrl).Result;

                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };
            }
            return response;
        }

        /// <summary>
        /// Gets Ecosystem Deployment Hosts
        /// </summary>
        /// <param name="sitekey"></param>
        /// <returns>HttpResponseMessage</returns>
        public static HttpResponseMessage GetEcosystemDeploymentHosts(string sitekey)
        {
            HttpResponseMessage response;
            try
            {
                using (var client = new HttpClient())
                {
                    if (!baseAddress.EndsWith(@"/"))
                    {
                        baseAddress += @"/";
                    }
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    // HTTP GET
                    string serviceUrl = "pma/facts/" + sitekey + "/hosts";
                    response = client.GetAsync(serviceUrl).Result;

                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };
            }
            return response;
        }

     
        //public static int PostSWDRequest(SWDRequestMsg swdRequest)
        public static HttpResponseMessage PostONEEmsTicketId(OneEMSCase swDeploymentReq)
        {
            HttpResponseMessage response;
            using (var client = new HttpClient())
            {
                if (!baseAddress.EndsWith(@"/"))
                {
                    baseAddress += @"/";
                }
                client.BaseAddress = new Uri(baseAddress);
                //client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string serviceUrl = "pma/in/dashboard/case_number";
                string jsonPost = "{\"siteid\": \"" + swDeploymentReq.siteid + "\", \"hostname\": \"" + swDeploymentReq.hostname 
                    + "\", \"service\": \"" + swDeploymentReq.service + "\", \"case_number\": \"" + swDeploymentReq.case_number + "\"}";
                response = client.PostAsJsonAsync(serviceUrl, jsonPost).Result;
                return response;
            }
        }

    }
}
