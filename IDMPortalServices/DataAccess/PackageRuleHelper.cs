﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Utilities;
using System.Collections.ObjectModel;
using log4net;
using System.Configuration;


namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public class PackageRuleHelper
    {
        private static ILog Logger = LogManager.GetLogger("IDM.Portal.DataAccess");
        private static string databaseName = ConfigurationManager.AppSettings["MongoDbDatabaseName"];
        private static string packageCollectionName = ConfigurationManager.AppSettings["MongoDbPackageCollectionName"];

        public void SavePackageRules(List<PackageRule> packageRules)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                //if (mongoDb.CollectionExists(packageCollectionName))
                //{
                //    mongoDb.DropCollection(packageCollectionName);
                //}
                if (packageRules.Count > 0)
                {
                    if (!mongoDb.CollectionExists(packageCollectionName))
                    {
                        mongoDb.CreateCollection(packageCollectionName);
                    }
                    var packageCollection = mongoDb.GetCollection(packageCollectionName);
                    foreach (var pkgRule in packageRules)
                    {
                        packageCollection.Update(
                            Query.EQ("package", pkgRule.package),
                            Update.Set("rules", new BsonArray(pkgRule.rules)),
                            new MongoUpdateOptions { CheckElementNames = true, Flags = UpdateFlags.Upsert }
                         );
                    }
                    //packageCollection.InsertBatch(packageRules);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateCountryRule(PackageRule rule)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                if (!mongoDb.CollectionExists(packageCollectionName))
                {
                    mongoDb.CreateCollection(packageCollectionName);
                }
                var packageCollection = mongoDb.GetCollection(packageCollectionName);
                packageCollection.Update(
                    Query.EQ("package", rule.package),
                    Update.Set("countries", new BsonArray(rule.countries)),
                    new MongoUpdateOptions { CheckElementNames = true, Flags = UpdateFlags.Upsert }
                    );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PackageRule> GetPackageRules()
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                var packageCollection = mongoDb.GetCollection(packageCollectionName);
                return packageCollection.FindAllAs<PackageRule>().SetFields(Fields.Exclude("_id")).ToList<PackageRule>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PackageRule GetPackageRulesByName(string packagename)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                var packageCollection = mongoDb.GetCollection(packageCollectionName);
                var packageColl = packageCollection.FindAs<PackageRule>(Query.EQ("package", packagename)).SetFields(Fields.Exclude("_id"));
                return packageColl.ToArray<PackageRule>()[0];
            }
            catch (Exception ex)
            {
                return null;
                //throw ex;
            }
        }
    }
}
