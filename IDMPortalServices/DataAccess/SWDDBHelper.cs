﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Utilities;
using System.Collections.ObjectModel;
using log4net;
using System.Configuration;


namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public class SWDDBHelper
    {
        private static ILog Logger = LogManager.GetLogger("IDM.Portal.DataAccess");
        private static string databaseName = ConfigurationManager.AppSettings["MongoDbDatabaseName"];
        private static string swdCollectionName = ConfigurationManager.AppSettings["MongoDbSWDCollectionName"];

        public void SaveSWDRequest(SWDRequestMsg swdRequest)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                if (!mongoDb.CollectionExists(swdCollectionName))
                {
                    //Making a capped collection of 100MB                    
                    mongoDb.CreateCollection(swdCollectionName);
                }
                var swdCollection = mongoDb.GetCollection(swdCollectionName);
                swdCollection.Insert(swdRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Old Implementation - This was done to pass the entire config required by the play book
        /// <summary>
        /// GetSWDConfiguration
        /// </summary>
        /// <returns></returns>
        public string GetSWDGridData()
        {
            var mongoClient = Utilities.GetSpecificCollection(databaseName, swdCollectionName);
            if (mongoClient != null)
            {
                var results = GetSWDConfiguration(mongoClient, string.Empty);
                List<SiteMapObjectID> allMessages = new List<SiteMapObjectID>();
                foreach (var doc in results)
                {
                    allMessages.Add(BsonSerializer.Deserialize<SiteMapObjectID>(doc));
                }

                List<SWDMessage> swdMessages = new List<SWDMessage>();
                int index = 0;
                foreach (SiteMapObjectID message in allMessages)
                {
                    index++;
                    swdMessages.Add(new SWDMessage()
                    {
                        _id = Convert.ToString(message._id),
                        swdid = index,
                        sitekey = message.sitemap.siteid,
                        sitename = message.sitemap.sitename,
                        status = message.sitemap.status
                    });
                }

                return swdMessages.ToJson();
            }
            return string.Empty;
        }

        /// <summary>
        /// GetSWDConfiguration
        /// </summary>
        /// <param name="siteid">siteid</param>
        /// <returns>all the software distribution configuration for a given site</returns>
        public string GetSWDConfiguration(string id)
        {
            var mongoClient = Utilities.GetSpecificCollection(databaseName, swdCollectionName);
            if (mongoClient != null)
            {
                UserFilter filter = new UserFilter();
                if (!string.IsNullOrEmpty(id))
                {
                    filter._id = id;
                }
                return mongoClient.Find(Utilities.BuildQuery(filter)).SetFields(Fields.Exclude("_id")).ToJson();
            }
            return string.Empty;
        }

        private static MongoCursor<BsonDocument> GetSWDConfiguration(MongoCollection<BsonDocument> mongoCollection, string _id)
        {
            UserFilter filter = new UserFilter();
            if (!string.IsNullOrEmpty(_id))
            {
                filter._id = _id;
            }
            var results = mongoCollection.Find(Utilities.BuildQuery(filter));
            return results;
        }

        public string InsertSWDConfiguration(SiteMapObject swdRequest)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                if (!mongoDb.CollectionExists(swdCollectionName))
                {
                    //Making a capped collection of 100MB                    
                    mongoDb.CreateCollection(swdCollectionName);
                }
                var swdCollection = mongoDb.GetCollection(swdCollectionName);
                swdCollection.Insert(swdRequest);
            }
            catch (Exception ex)
            {
                //TODO: handle
            }
            return "OK";
        }

        public MongoCursor<SWDRequestMsg> GetAllSWDRequests(SearchCriteria criteria)
        {
            UserFilter filter;
            var swdCollection = Utilities.GetSpecificCollection(databaseName, swdCollectionName);
            try
            {
                filter = new UserFilter();
                return GetSWDRequest(swdCollection, filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filter = null;
            }            
        }

        private MongoCursor<SWDRequestMsg> GetSWDRequest(MongoCollection<BsonDocument> mongoCollection, UserFilter filter)
        {
            return mongoCollection.FindAs<SWDRequestMsg>(Utilities.BuildQuery(filter)).SetFields(Fields.Exclude("_id")).SetSortOrder(
                SortBy.Descending(new string[] { "createddate"} ));
        }
    }
}
