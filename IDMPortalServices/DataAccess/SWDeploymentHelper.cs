﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Utilities;
using System.Collections.ObjectModel;
using log4net;
using System.Configuration;


namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public class SWDeploymentHelper
    {
        private static ILog Logger = LogManager.GetLogger("IDM.Portal.DataAccess");
        private static string databaseName = ConfigurationManager.AppSettings["MongoDbDatabaseName"];
        private static string deploymentCollectionName = ConfigurationManager.AppSettings["MongoDbSWDeployCollectionName"];

        public void SaveSWDeploymentRequest(SWDeployment deploymentRequest)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                if (!mongoDb.CollectionExists(deploymentCollectionName))
                {
                    //Making a capped collection of 100MB                    
                    mongoDb.CreateCollection(deploymentCollectionName);
                }
                var swdCollection = mongoDb.GetCollection(deploymentCollectionName);
                swdCollection.Insert(deploymentRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
                 
        /// <summary>
        /// GetSWDeploymentGridData
        /// </summary>
        /// <returns></returns>
        public string GetSWDeploymentGridData()
        {
            var mongoClient = Utilities.GetSpecificCollection(databaseName, deploymentCollectionName);
            if (mongoClient != null)
            {
                var results = GetSWDeploymentConfiguration(mongoClient, string.Empty);
                List<SiteMapObjectID> allMessages = new List<SiteMapObjectID>();
                foreach (var doc in results)
                {
                    allMessages.Add(BsonSerializer.Deserialize<SiteMapObjectID>(doc));
                }

                List<SWDMessage> swdMessages = new List<SWDMessage>();
                int index = 0;
                foreach (SiteMapObjectID message in allMessages)
                {
                    index++;
                    swdMessages.Add(new SWDMessage()
                    {
                        _id = Convert.ToString(message._id),
                        swdid = index,
                        sitekey = message.sitemap.siteid,
                        sitename = message.sitemap.sitename,
                        status = message.sitemap.status
                    });
                }

                return swdMessages.ToJson();
            }
            return string.Empty;
        }

        /// <summary>
        /// GetSWDeploymentConfiguration
        /// </summary>
        /// <param name="siteid">id</param>
        /// <returns>all the software distribution configuration for a given site</returns>
        public string GetSWDeploymentConfiguration(string id)
        {
            var mongoClient = Utilities.GetSpecificCollection(databaseName, deploymentCollectionName);
            if (mongoClient != null)
            {
                UserFilter filter = new UserFilter();
                if (!string.IsNullOrEmpty(id))
                {
                    filter._id = id;
                }
                return mongoClient.Find(Utilities.BuildQuery(filter)).SetFields(Fields.Exclude("_id")).ToJson();
            }
            return string.Empty;
        }

        /// <summary>
        /// GetSWDeploymentConfiguration
        /// </summary>
        /// <param name="mongoCollection"></param>
        /// <param name="_id"></param>
        /// <returns>BsonDocument</returns>
        private static MongoCursor<BsonDocument> GetSWDeploymentConfiguration(MongoCollection<BsonDocument> mongoCollection, string _id)
        {
            UserFilter filter = new UserFilter();
            if (!string.IsNullOrEmpty(_id))
            {
                filter._id = _id;
            }
            var results = mongoCollection.Find(Utilities.BuildQuery(filter));
            return results;
        }

        /// <summary>
        /// InsertSWDeploymentConfiguration
        /// </summary>
        /// <param name="swdRequest"></param>
        /// <returns>string</returns>
        public string InsertSWDeploymentConfiguration(SiteMapObject swdRequest)
        {
            var client = Utilities.GetConnection();
            if (client == null)
            {
                //TODO: Write to a file
            }
            try
            {
                //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
                var mongoDb = client.GetServer().GetDatabase(databaseName);
                if (!mongoDb.CollectionExists(deploymentCollectionName))
                {
                    //Making a capped collection of 100MB                    
                    mongoDb.CreateCollection(deploymentCollectionName);
                }
                var swdCollection = mongoDb.GetCollection(deploymentCollectionName);
                swdCollection.Insert(swdRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "OK";
        }

        /// <summary>
        /// GetAllSWDeploymentRequests
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns>SWDRequestMsg</returns>
        public MongoCursor<SWDeployment> GetAllSWDeploymentRequests(SearchCriteria criteria)
        {
            UserFilter filter;
            var swdCollection = Utilities.GetSpecificCollection(databaseName, deploymentCollectionName);
            try
            {
                filter = new UserFilter();
                return GetSWDeploymentRequest(swdCollection, filter);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filter = null;
            }
        }

        /// <summary>
        /// GetSWDeploymentRequest
        /// </summary>
        /// <param name="mongoCollection"></param>
        /// <param name="filter"></param>
        /// <returns>MongoCursor<SWDRequestMsg></returns>
        private MongoCursor<SWDeployment> GetSWDeploymentRequest(MongoCollection<BsonDocument> mongoCollection, UserFilter filter)
        {
            return mongoCollection.FindAs<SWDeployment>(Utilities.BuildQuery(filter)).SetFields(Fields.Exclude("_id")).SetSortOrder(
                SortBy.Descending(new string[] { "createddate" }));
        }
    } 
}