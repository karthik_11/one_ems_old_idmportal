﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Utilities;
using System.Collections.ObjectModel;
using log4net;
using System.Configuration;

namespace Philips.HealthCare.IDM.Portal.DataAccess
{
    public static class Utilities
    {
        private static ILog Logger = LogManager.GetLogger("IDM.Portal.DataAccess");

        public static MongoCollection<BsonDocument> GetSpecificCollection(string databaseName, string collectionName)
        {
            MongoDatabase mongoDB = null;
            MongoCollection<BsonDocument> mongoCollection = null;
            try
            {
                mongoDB = GetSpecificDatabase(databaseName);
                if (mongoDB != null)
                {
                    mongoCollection = mongoDB.GetCollection<BsonDocument>(collectionName);
                }
                return mongoCollection;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error while trying to read the get the collection - " + collectionName + " from the Mongo DB - " + databaseName, ex));
                return null;
            }
        }

        public static MongoDatabase GetSpecificDatabase(string databaseName)
        {
            MongoClient mongoClient;
            MongoDatabase mongoDB = null;
            try
            {
                mongoClient = GetConnection();
                if (mongoClient != null)
                {
                    mongoDB = mongoClient.GetServer().GetDatabase(databaseName);
                }
                return mongoDB;
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Error while trying to read the get the connection for the Mongo DB - " + databaseName, ex));
                return null;
            }
        }

        public static MongoClient GetConnection()
        {
            try
            {
                var connectionString = ConfigurationManager.AppSettings["MongoDbConnectionString"];
                return new MongoClient(connectionString);
            }
            catch (ConfigurationErrorsException ex)
            {
                Logger.Error(string.Format("Error while trying to read the MongoDBConnectionString from the config file", ex));
                return null;
            }
        }

        public static IMongoQuery BuildQuery(UserFilter filter)
        {
            var queries = new List<IMongoQuery>();
            if (!String.IsNullOrEmpty(filter._id))
            {
                ObjectId objID = new ObjectId(filter._id);
                queries.Add(Query.EQ("_id", objID));
            }
            if (!String.IsNullOrEmpty(filter.siteid))
            {
                queries.Add(Query.EQ("siteid", filter.siteid));
            }
            if (!String.IsNullOrEmpty(filter.hostname))
            {
                queries.Add(Query.EQ("hostname", filter.hostname));
            }
            if (filter.service != null && filter.service != string.Empty)
            {
                queries.Add(Query.EQ("service", filter.service));
            }
            if (filter.serviceList != null)
            {
                queries.Add(Query.In("service", new BsonArray(filter.serviceList)));
            }
            if (!String.IsNullOrEmpty(filter.status))
            {
                queries.Add(Query.EQ("status", filter.status));
            }
            if (!String.IsNullOrEmpty(filter.type))
            {
                queries.Add(Query.EQ("type", filter.type));
            }
            if (filter.beginTime != DateTime.MinValue)
            {
                queries.Add(Query.GTE("timestamp", filter.beginTime));
            }
            if (filter.endTime != DateTime.MinValue)
            {
                queries.Add(Query.LTE("timestamp", filter.endTime));
            }
            if (!String.IsNullOrEmpty(filter.hostaddress))
            {
                queries.Add(Query.EQ("hostaddress", filter.hostaddress));
            }
            if (queries.Count == 0)
            {
                return null;
            }
            var queryBuilder = new QueryBuilder<BsonDocument>();
            return queryBuilder.And(queries);
        }
    }
}
