﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// HostInformation
    /// </summary>
    public class HostInformation {
        /// <summary>
        /// HostName 
        /// </summary>
        public string HostName { get; set; }
        /// <summary>
        /// TotalCount 
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// SuccessCount 
        /// </summary>
        public int SuccessCount { get; set; }
        /// <summary>
        /// FailureCount 
        /// </summary>
        public int FailureCount { get; set; }
    }

    /// <summary>
    /// SiteInformation
    /// </summary>
    public class SiteInformation {
        /// <summary>
        /// SiteID
        /// </summary>
        public string SiteID { get; set; }
        /// <summary>
        /// TotalCount 
        /// </summary>
        public int TotalCount { get; set; }
        /// <summary>
        /// SuccessCount 
        /// </summary>
        public int SuccessCount { get; set; }
        /// <summary>
        /// FailureCount 
        /// </summary>
        public int FailureCount { get; set; }
        /// <summary>
        /// HostInformation 
        /// </summary>
        public List<HostInformation> HostInformation { get; set; }        
    }
}
