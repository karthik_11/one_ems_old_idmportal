﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// perfdata
    /// </summary>
    [Serializable]
    [DataContract]
    public class Perfdata {
        /// <summary>
        /// critical
        /// </summary>
        [DataMember]
        public string crit { get; set; }
        /// <summary>
        /// key
        /// </summary>
        [DataMember]
        public string key { get; set; }
        /// <summary>
        /// max
        /// </summary>
        [DataMember]
        public string max { get; set; }
        /// <summary>
        /// min
        /// </summary>
        [DataMember]
        public string min { get; set; }
        /// <summary>
        /// uom
        /// </summary>
        [DataMember]
        public string uom { get; set; }
        /// <summary>
        /// value
        /// </summary>
        [DataMember]
        public string value { get; set; }
        /// <summary>
        /// warn
        /// </summary>
        [DataMember]
        public string warn { get; set; }
        /// <summary>
        /// label
        /// </summary>
        [DataMember]
        public string label { get; set; }
    }
}