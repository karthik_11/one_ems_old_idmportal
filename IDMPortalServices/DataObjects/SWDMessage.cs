﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    public class SWDMessage
    {
        /// <summary>
        /// _id
        /// </summary>
        public object _id { get; set; }
        /// <summary>
        /// id
        /// </summary>
        public int swdid { get; set; }
        /// <summary>
        /// sitekey
        /// </summary>
        public string sitekey { get; set; }
        /// <summary>
        /// sitename
        /// </summary>
        public string sitename { get; set; }
        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }
    }
}
