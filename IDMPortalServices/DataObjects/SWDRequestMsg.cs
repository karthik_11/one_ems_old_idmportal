﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
    public class SWDRequestMsg    
    {
        public SWDRequestMsg()
        {
            rules = new List<string>();
            sites = new List<string>();
            countries = new List<string>();
        }
        /// <summary>
        /// pkg name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// pkg location
        /// </summary>
        public string location { get; set; }
        /// <summary>
        /// version
        /// </summary>
        public string version { get; set; }
        /// <summary>
        /// rules
        /// </summary>
        public List<string> rules { get; set; }     
        /// <summary>
        /// list of sites
        /// </summary>
        public List<string> sites { get; set; }
        /// <summary>
        /// countries
        /// </summary>
        public List<string> countries { get; set; }
        /// <summary>
        /// pkg status
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// createdby
        /// </summary>
        public string createdby { get; set; }        
        ///// <summary>
        ///// createddate
        ///// </summary>
       // [BsonDateTimeOptions(Representation = BsonType.String, Kind = DateTimeKind.Utc)]
        public DateTime createddate { get; set; }
        ///// <summary>
        ///// createddate
        ///// </summary>        
        public string createddatestring
        {
            get
            {
                return createddate.ToString("dd MMM yyyy HH:mm:ss tt");
            }
        }
        /// <summary>
        /// modifiedby
        /// </summary>
        public string modifiedby { get; set; }
        /// <summary>
        /// lastmodified
        /// </summary>
       // [BsonDateTimeOptions(Representation = BsonType.String, Kind = DateTimeKind.Utc)]
        public DateTime lastmodified { get; set; }
    }

    public class SWDJsonPost
    {
        public IList<string> sites;
        public IList<string> subscriptions;
    }

    //public class Product
    //{
    //    public string name { get; set; }
    //    public string type {get; set;}
    //    public string version { get; set; }
    //    public string location { get; set; }
    //}

    public class SWDProduct
    {
        public IList<string> subscriptions;
    }


    
}
