﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// SearchCriteria
    /// </summary>
    public class SearchCriteria {
        /// <summary>
        /// siteid
        /// </summary>
        public string siteid { get; set; }
        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// service
        /// </summary>
        public string service { get; set; }
        /// <summary>
        /// startDate
        /// </summary>
        public DateTime startDate { get; set; }
        /// <summary>
        /// endDate
        /// </summary>
        public DateTime endDate { get; set; }        
        /// <summary>
        /// packagename
        /// </summary>
        public string packagename { get; set; }
        /// <summary>
        /// country
        /// </summary>
        public string country { get; set; }
    }

    public class PostJSONPayload
    {
        public string key { get; set; }
        public string payload { get; set; }        
    }
}
