﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects
{
  
    /// <summary>
    /// Contract for Site Exception Request Message
    /// </summary>
    public class SiteExceptionReqMsg
    {
        /// <summary>
        /// Constructor for Site Exception Request Message
        /// </summary>
        public SiteExceptionReqMsg()
        {            
        }

        /// <summary>
        /// siteid
        /// </summary>
        public string siteid { get; set; }

        /// <summary>
        /// site name
        /// </summary>
        public string sitename { get; set; }

         /// <summary>
        /// site status
        /// </summary>
        public string sitestatus { get; set; }

        /// <summary>
        /// site version
        /// </summary>
        public string siteversion { get; set; }
         

        /// <summary>
        /// production / Non production
        /// </summary>
        public string production { get; set; }

        /// <summary>
        /// country
        /// </summary>
        public string country { get; set; }

        ///// <summary>
        ///// perfdata
        ///// </summary>     
        //public List<Perfdata> perfdata { get; set; }        
    }    
}
