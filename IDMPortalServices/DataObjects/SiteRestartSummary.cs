﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    public class SiteRestartSummary {
      
        public string siteid { get; set; }
        public string sitename { get; set; }
        public long totalattempts { get; set; }
        public long successattempts { get; set; }
        public long failureattempts { get; set; }
        public List<HostRestartSummary> hostrestartsummary { get; set; }

        public SiteRestartSummary() {
            hostrestartsummary = new List<HostRestartSummary>();
        }
    }

    public class HostRestartSummary {
        public string hostname {get;set;}
        public long totalattempts { get; set; }
        public long successattempts { get; set; }
        public long failureattempts { get; set; }
    }
}

