﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace Philips.HealthCare.IDM.Portal.DataObjects {
    /// <summary>
    /// UserFilter
    /// </summary>
    public class UserFilter {
        /// <summary>
        /// _id
        /// </summary>
        public string _id { get; set; }
        /// <summary>
        /// hostaddress
        /// </summary>
        public string hostaddress { get; set; }
        /// <summary>
        /// hostname
        /// </summary>
        public string hostname { get; set; }
        /// <summary>
        /// service
        /// </summary>
        public string service { get; set; }
        /// <summary>
        /// type
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// status
        /// </summary>
        public string status { get; set; }
        /// <summary>
        /// siteid
        /// </summary>
        public string siteid { get; set; }
        /// <summary>
        /// output
        /// </summary>
        public string output { get; set; }
        /// <summary>
        /// dateTimerange
        /// </summary>
        public string dateTimeRange { get; set; }
        /// <summary>
        /// beginTime
        /// </summary>
        public DateTime beginTime { get; set; }
        /// <summary>
        /// endTime
        /// </summary>
        public DateTime endTime { get; set; }
        /// <summary>
        /// public IEnumerable<string> serviceList { get; set; }
        /// </summary>
        public IEnumerable<BsonValue> serviceList { get; set; }

        /// <summary>
        /// packagename
        /// </summary>
        public string packagename { get; set; }
        /// <summary>
        /// country
        /// </summary>
        public string country { get; set; }
    }
}