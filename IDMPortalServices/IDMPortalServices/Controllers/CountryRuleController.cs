﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class CountryRuleController : ApiController
    {
        PackageRuleHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="PackageRuleController"/> class.
        /// </summary>
        public CountryRuleController()
        {
            helper = new PackageRuleHelper();
        }

        public HttpResponseMessage Post([FromBody] PackageRule packageRule)
        {
            responseObject = new ResponseObject();
            try
            {
                if (packageRule == null)
                {
                    throw new Exception("Package Rule is empty");
                }
                helper.UpdateCountryRule(packageRule);
                responseObject.status = 1;
                responseObject.message = "Rule created successfully";
                responseObject.payload = JsonConvert.SerializeObject(packageRule);
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}