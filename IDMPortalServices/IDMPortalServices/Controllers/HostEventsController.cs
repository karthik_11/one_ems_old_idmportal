﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class HostEventsController : ApiController {
         MongoDbHelper helper;        

        /// <summary>
         /// Initializes a new instance of the <see cref="HostEventsController"/> class.
        /// </summary>
         public HostEventsController() {
            helper = new MongoDbHelper();
        }

        /// <summary>
        /// The default Host Events 
        /// </summary>
        /// <returns>all the host info that are CRITICAL</returns>
         public HttpResponseMessage Get() {
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of HostEventsController" });
             var json = JArray.Parse(helper.GetDefaultHostEvents());
             return new HttpResponseMessage() {
                 Content = new JsonContent(json)
             };
         }
    }
}