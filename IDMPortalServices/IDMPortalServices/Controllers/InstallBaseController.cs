﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.Logger;
using System.Net.Http.Headers;
using System.Net;
using MongoDB.Bson;
using System.Net.Http.Formatting;
using Philips.HealthCare.IDM.Portal.DataAccess;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class InstallBaseController : ApiController
    {
        
        //public HttpResponseMessage Get()
        //{
        ////    LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of InstallBaseController " });

        ////    // var json = "{'subscriptions': ['product1', 'product2', 'product3', 'product4', 'product5', 'product6', 'product7', 'product77', 'productX']}";

        //  //  var json = "{'results': [{'siteid': 'OPS02', 'components': []}, {'siteid': 'ipt01', 'components': []}]}";

        //    var json = "{ 'results': [{'siteid': 'BENHC','components': [{'name': 'IntelliSpace Visible Light','value': '2.1.12.0' },{ 'name': 'Windows', 'value': 'Microsoft Windows Server 2008 R2 Standard , Service Pack 1' }, { 'name': 'IntelliSpace Anywhere',  'value': '1.3.12.0' } ]  }, { 'siteid': 'test0', 'components': [ {'name': 'IntelliSpace Visible Light',                    'value': '2.1.12.0'},{'name': 'Windows','value': 'Microsoft Windows Server 2008 R2 Standard , Service Pack 1'}, {'name': 'IntelliSpace Anywhere','value': '1.3.12.0' } ] }]}";

        //    return new HttpResponseMessage()
        //    {
        //        Content = new JsonContent(JObject.Parse(json))
        //    };
        //}
 
        public HttpResponseMessage Get()
        {
            HttpResponseMessage response;
            try
            {
                var result = string.Empty;
                JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
                response = EcosystemAPIHelper.GetEcosystemInstallBasePackages();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result = response.Content.ReadAsStringAsync().Result;
            
                    response.Content = new ObjectContent(result.GetType(), result, objFormatter);
            
                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
            };
        }
            return response;          
        }
         
        
    }
}
