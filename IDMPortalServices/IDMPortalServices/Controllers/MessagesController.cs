﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class MessagesController : ApiController
    {
        MongoDbHelper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagesController"/> class.
        /// </summary>
        public MessagesController(){
            helper = new MongoDbHelper();
        }

        // GET api/<controller>
        /// <summary>
        /// Get
        /// </summary>
        /// <returns>All the Messages</returns>
        public HttpResponseMessage Get() {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Messages Controller" });
            var json = JArray.Parse(helper.GetAllMessages());
            return new HttpResponseMessage() {
                Content = new JsonContent(json)
            };
        }

        // GET api/<controller>/5
        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        //public string Get(int id) {
        //  return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value) {
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value) {
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id) {
        //}
    }
}