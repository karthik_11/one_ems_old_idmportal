﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using MongoDB.Bson;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class NamespaceController : ApiController {

        MongoDbHelper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="NamespaceController"/> class.
        /// </summary>
        public NamespaceController() {
            helper = new MongoDbHelper();
        }

        public HttpResponseMessage Get() {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Namespace Controller" });
            var json = JArray.Parse(helper.GetAllServiceNamespaces().ToJson());
            return new HttpResponseMessage() {
                Content = new JsonContent(json)
            };
        }
    }
}