﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class NodesController : ApiController
	{
         MongoDbHelper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessagesController"/> class.
        /// </summary>
         public NodesController() {
            helper = new MongoDbHelper();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
         public HttpResponseMessage Get() {
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Messages Controller" });
             var json = JArray.Parse(helper.GetAllNodes());
             return new HttpResponseMessage() {
                 Content = new JsonContent(json)
             };
         }

        /// <summary>
         /// Message
        /// </summary>
        /// <param name="id"></param>
        /// <returns>the all the messages for a given node</returns>
        [AcceptVerbs("GET")]
         public HttpResponseMessage Message(string id) {
             LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Messages Controller" });
             var json = JArray.Parse(helper.GetAllMessagesForNode(id));
             return new HttpResponseMessage() {
                 Content = new JsonContent(json)
             };
         }
	}
}