﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class PackageRuleController : ApiController
    {
        PackageRuleHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="PackageRuleController"/> class.
        /// </summary>
        public PackageRuleController()
        {
            helper = new PackageRuleHelper();
        }

        // <summary>
        // FilteredMessage
        // </summary>
        // <param name="criteria"></param>
        // <returns>all the messages based upon the criteria</returns>
        public HttpResponseMessage Post([FromBody] List<PackageRule> packageRules)
        {
            responseObject = new ResponseObject();
            try
            {
                if (packageRules == null)
                {
                    throw new Exception("Package Rules are empty");
                }
                helper.SavePackageRules(packageRules);
                responseObject.status = 1;
                responseObject.message = "PackageRule";
                responseObject.payload = JsonConvert.SerializeObject(packageRules);
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}