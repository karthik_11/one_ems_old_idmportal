﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataObjects;
using MongoDB.Bson;
using System.IO;
using System.Net.Http.Formatting;
using Philips.HealthCare.IDM.Portal.DataAccess;
using System.Net.Http.Headers;
using System.Net;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SWDPackagesController : ApiController
    {
        public HttpResponseMessage Get()
        {  
            List<SWDRequestMsg> result;
            JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();            
            HttpResponseMessage response = GetSWDPackages(out result);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                //https://social.msdn.microsoft.com/Forums/office/en-US/b79b75f4-b46b-46ae-ae29-17a352b6b90b/custom-http-response-headers-for-sp-2013-shown-2-times?forum=sharepointdevelopment
                if (response.Headers.Contains("Access-Control-Allow-Origin"))
                    response.Headers.Remove("Access-Control-Allow-Origin");
                response.Content = new ObjectContent(result.GetType(), result, objFormatter);
            }            
            return response;
        }

        private HttpResponseMessage GetSWDPackages(out List<SWDRequestMsg> packages)
        {
            HttpResponseMessage response;
            packages = new List<SWDRequestMsg>();
            try
            {
                response = EcosystemAPIHelper.GetEcosystemPackages();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    PackageRuleHelper helper = new PackageRuleHelper();
                    List<PackageRule> packageRules = helper.GetPackageRules();

                    foreach (string subscription in response.Content.ReadAsAsync<SWDProduct>().Result.subscriptions)
                    {
                        SWDRequestMsg swdRequestMsg = new SWDRequestMsg();
                        swdRequestMsg.name = subscription;
                        swdRequestMsg.rules.Add("singlesitedeployment");
                        foreach (PackageRule pkgRule in packageRules)
                        {
                            if (swdRequestMsg.name == pkgRule.package)
                            {
                                if (pkgRule.rules != null)
                                {
                                    swdRequestMsg.rules.Clear();
                                    swdRequestMsg.rules.AddRange(pkgRule.rules);
                                }                                
                                if (pkgRule.countries != null)
                                {
                                    swdRequestMsg.countries.Clear();
                                    swdRequestMsg.countries.AddRange(pkgRule.countries);
                                }                                
                            }
                        }
                        packages.Add(swdRequestMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(ex.Message)
                };  
            }
            return response;
        }
    }
}