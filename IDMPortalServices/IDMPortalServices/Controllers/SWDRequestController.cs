﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.Logger;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Newtonsoft.Json;
using Philips.HealthCare.IDM.Portal.IDM_ESBInterface;
using MongoDB.Bson;
using System.Net;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SWDRequestController : ApiController
    {
        SWDDBHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SWDRequestController"/> class.
        /// </summary>
        public SWDRequestController()
        {
            helper = new SWDDBHelper();
        }

        public HttpResponseMessage Get()
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of SWD Controller" });
            var json = JArray.Parse(helper.GetSWDGridData());
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }

        [AcceptVerbs("GET")]
        public HttpResponseMessage Get(string id)
        {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method with parameter of SWD Controller" });
            var json = JArray.Parse(helper.GetSWDConfiguration(id));
            return new HttpResponseMessage()
            {
                Content = new JsonContent(json)
            };
        }

        // <summary>
        // FilteredMessage
        // </summary>
        // <param name="criteria"></param>
        // <returns>all the messages based upon the criteria</returns>
        public HttpResponseMessage Post([FromBody] SWDRequestMsg swdRequest)
        {
            HttpResponseMessage response;
            responseObject = new ResponseObject();
            try
            {
                if (swdRequest == null)
                {
                    throw new Exception("Invalid SWD Request.");
                }
                var postSWDJson = new SWDJsonPost() { sites = swdRequest.sites, subscriptions = new List<string>() { swdRequest.name } };
                response = EcosystemAPIHelper.PostSWDRequest(postSWDJson);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    helper.SaveSWDRequest(swdRequest);
                    responseObject.status = 1;
                    responseObject.message = "Software Distribution request created successfully to the IDM Ecosystem.";
                    responseObject.payload = JsonConvert.SerializeObject(swdRequest);
                }
                else
                {
                    responseObject.status = 0;
                    responseObject.message = "Error in submitting Software Distribution request to the IDM Ecosystem.";
                    responseObject.payload = JsonConvert.SerializeObject(swdRequest);
                }                
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {  
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }
    }
}