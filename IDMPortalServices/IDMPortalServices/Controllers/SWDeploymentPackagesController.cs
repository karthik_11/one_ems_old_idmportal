﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using MongoDB.Bson;
using System.IO;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net;
using MongoDB.Driver;
using Philips.HealthCare.IDM.Portal.DataObjects;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers
{
    public class SWDeploymentPackagesController : ApiController
    { 
        ResponseObject responseObject;
        SWDeploymentHelper helper;  

         /// <summary>
        /// Initializes a new instance of the <see cref="SWDeploymentPackagesController"/> class.
        /// </summary>
        public SWDeploymentPackagesController()
        {
            helper = new SWDeploymentHelper();
        }

        /// <summary>
        /// Get Method to Retrive the Software Deployment Packages 
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            JsonMediaTypeFormatter objFormatter = new JsonMediaTypeFormatter();
            SearchCriteria criteria = new SearchCriteria();
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Software Deployment Controller" });
            MongoCursor<SWDeployment> result = helper.GetAllSWDeploymentRequests(criteria);
            var httpResponseMessage = new HttpResponseMessage()
            {
                Content = new ObjectContent(result.GetType(), result, objFormatter)
            };
            return httpResponseMessage;
        }    
               
        [HttpPost]
        [ActionName("SiteInfoDeployment")]
        public HttpResponseMessage SiteInfoDeployment([FromBody] string sitekey)
        {
            responseObject = new ResponseObject();
            try
            {               
                var path = @"\IDMPortalServices\swdeploymentpackages.json";
                StreamReader r = new StreamReader(path);
                string payload = r.ReadToEnd();

                LogManager.LogWriter.WriteLog(new LogMessage() { Description = "Get the Site Info for deployment" });

                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = payload;
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }
            return new HttpResponseMessage()
            {
                Content = new JsonContent(JObject.Parse(JsonConvert.SerializeObject(responseObject)))
            };
        }              
     }
}