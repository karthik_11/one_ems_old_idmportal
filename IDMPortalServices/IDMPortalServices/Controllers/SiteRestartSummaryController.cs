﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class SiteRestartSummaryController : ApiController {
         MongoDbHelper helper;

        /// <summary>
         /// Initializes a new instance of the <see cref="SiteRestartSummaryController"/> class.
        /// </summary>
         public SiteRestartSummaryController() {
            helper = new MongoDbHelper();
        }


         /// <summary>
         /// FilteredMessage
         /// </summary>
         /// <param name="criteria"></param>
         /// <returns>all the messages based upon the criteria</returns>
         public HttpResponseMessage Post([FromBody] SearchCriteria criteria) {
             JavaScriptSerializer serializer = new JavaScriptSerializer();
             var json = JArray.Parse(serializer.Serialize(helper.RestartSummaryCount(criteria)));
             return new HttpResponseMessage() {
                 Content = new JsonContent(json)
             };
         }
        
    }
}