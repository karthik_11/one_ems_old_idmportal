﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class SystemFiltersController : ApiController {

        MongoDbHelper helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemFiltersController"/> class.
        /// </summary>
        public SystemFiltersController() {
            helper = new MongoDbHelper();
        }


        [AcceptVerbs("GET")]
        public HttpResponseMessage Filters() {
            LogManager.LogWriter.WriteLog(new LogMessage() { Description = "get method of Namespace Controller" });
            //JObject output = JObject.FromObject(helper.GetServiceFilters());

            var json = JArray.Parse(helper.GetServiceFilters());
            return new HttpResponseMessage() {
                Content = new JsonContent(json)
            };

            //var json = helper.GetSystemFilters();
            //return new HttpResponseMessage() {
            //    Content = new JsonContent(output)
            //};
        }
    }
}
