﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.Logger;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Philips.HealthCare.IDM.Portal.DataObjects;

namespace Philips.HealthCare.IDM.Portal.Services.Controllers {
    public class ThrukViewController : ApiController {

        MongoDbHelper helper;
        ResponseObject responseObject;

        /// <summary>
        /// Initializes a new instance of the <see cref="SitesController"/> class.
        /// </summary>
        public ThrukViewController() {
            helper = new MongoDbHelper();
            responseObject = new ResponseObject();
        }

        /// <summary>
        /// FilteredMessage
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns>all the messages based upon the criteria</returns>
        public HttpResponseMessage Post([FromBody] string siteKey) {
            try
            {
                string url = helper.GetThrukViewUrl(siteKey);
                responseObject.status = 1;
                responseObject.message = "success";
                responseObject.payload = url;
            }
            catch (Exception ex)
            {
                responseObject.status = 0;
                responseObject.message = ex.Message;
                responseObject.payload = string.Empty;
            }

            return new HttpResponseMessage()
            {
                Content = new JsonContent(
                    JObject.Parse(JsonConvert.SerializeObject(responseObject))
                    )
            };
        }
    }
}