    Param
	(			
        [Parameter(Mandatory=$true, HelpMessage="Specify the Mongo DB Server IP ")]
		[alias("m")]
        $mdb 
		,
		[Parameter(Mandatory=$true, HelpMessage="Specify the IDM Ecosystem Server IP ")]
		[alias("e")]
        $idm 
		,
		[Parameter(Mandatory=$true, HelpMessage="Specify OneEMS Reference - DEVELOPMENT OR PRODUCTION")]        		
        [string]
		[ValidateSet("Development", "Production")] 
		[alias("t")]
		$ems
		,
		[Parameter(Mandatory=$true,HelpMessage="Specify the Sales Force UserName")]        
        [string]
		[alias("u")]
		$user
		,
		[Parameter(Mandatory=$true, HelpMessage="Specify the Sales Force Password")]        
        [string]
		[alias("p")]
		$pass
		,
		[Parameter(Mandatory=$false, HelpMessage="Specify the App Pool User Name. EX:- Domain\Username ")]
		[alias("au")]
        $appuser
		,
		[Parameter(Mandatory=$false, HelpMessage="Specify the App Pool Password ")]
		[alias("ap")]
        $apppass
    ) 

	# Write-Host 'Deploying IDM Portal...'  	
	# --------------------------------------------------------------------
	# Checking Execution Policy
	# --------------------------------------------------------------------
	#$Policy = "Unrestricted"
	$Policy = "RemoteSigned"
	If ((get-ExecutionPolicy) -ne $Policy) {
	  Write-Host "Script Execution is disabled. Enabling it now"
	  Set-ExecutionPolicy $Policy -Force
	  Write-Host "Please Re-Run this script in a new powershell enviroment"
	  Exit
	}

	# --------------------------------------------------------------------
	# Define the variables.
	# --------------------------------------------------------------------
	Write-Host "Defining Variables..."
	$InetPubRoot = "C:\Inetpub"
	$InetPubLog = "C:\Inetpub\Log"
	$InetPubWWWRoot = "C:\Inetpub\WWWRoot"

	# --------------------------------------------------------------------
	# Loading Feature Installation Modules
	# --------------------------------------------------------------------
	Write-Host "Loading the Server Manager..."
	Import-Module ServerManager 

	# --------------------------------------------------------------------
	# Installing IIS
	# --------------------------------------------------------------------
	Write-Host "Adding Windows Features..."
	Add-WindowsFeature -Name Web-Common-Http,Web-Http-Logging,Web-Custom-Logging,Web-Log-Libraries,Web-ODBC-Logging,Web-Request-Monitor,Web-Http-Tracing,Web-Performance,
	Web-Security,Web-Basic-Auth,Web-CertProvider,Web-Client-Auth,Web-Digest-Auth,Web-Cert-Auth,Web-IP-Security,Web-Url-Auth,Web-Windows-Auth,Web-App-Dev,Web-Net-Ext45,Web-Asp-Net45,
	Web-ISAPI-Ext,Web-ISAPI-Filter,Web-Mgmt-Tools,Web-Mgmt-Console

	# --------------------------------------------------------------------
	# Loading IIS Modules
	# --------------------------------------------------------------------
	Write-Host "Loading Web Administration..."
	Import-Module WebAdministration
	
	$iisAppPoolName = "idmapppool"	

	# --------------------------------------------------------------------
	# Creating IIS Folder Structure
	# --------------------------------------------------------------------
	Write-Host "Creating Web Directories..."
	New-Item -Path $InetPubRoot -type directory -Force -ErrorAction SilentlyContinue
	New-Item -Path $InetPubLog -type directory -Force -ErrorAction SilentlyContinue
	New-Item -Path $InetPubWWWRoot -type directory -Force -ErrorAction SilentlyContinue

	# --------------------------------------------------------------------
	# Remove old app
	# --------------------------------------------------------------------
	Write-Host "Deleting the Old application..."
	Remove-Item 'IIS:\Sites\Default Web Site\IDMPortalServices' -recurse -ErrorAction SilentlyContinue
	Remove-Item 'IIS:\Sites\Default Web Site\idmapp' -recurse -ErrorAction SilentlyContinue

	# --------------------------------------------------------------------
	# Copying files from build folder
	# --------------------------------------------------------------------

	$IDMPortalServices = $InetPubWWWRoot + "\IDMPortalServices"
	$idmapp = $InetPubWWWRoot + "\idmapp"

	# New-Item -Path $IDMPortalServices -type directory -Force -ErrorAction SilentlyContinue
	# New-Item -Path $idmapp -type directory -Force -ErrorAction SilentlyContinue

	# --------------------------------------------------------------------
	# Creating Web Applications
	# --------------------------------------------------------------------
	
	#check if the app pool exists
	# if (!(Test-Path $iisAppPoolName -pathType container))
	if(!(Test-Path ("IIS:\AppPools\" + $iisAppPoolName)))
	{
		#create the app pool
		Write-Host "Creating IDMAppPool"
		$appPool = New-Item ("IIS:\AppPools\" + $iisAppPoolName)

		if($appuser) 
		{					
			Write-Host "Inside loop"
			Write-Host $appuser
			$apppool.processModel.username = $appuser
			$apppool.processModel.password = $apppass
			$apppool.processModel.identityType = "SpecificUser"
		}
		$appPool.managedRuntimeVersion = "v4.0"
		$appPool.managedPipelineMode = "Integrated"
		$appPool | Set-Item		
	}

	Write-Host "Creating the idmapp and portal services application..."
	$iisApp = New-Item 'IIS:\Sites\Default Web Site\IDMPortalServices' -ApplicationPool $iisAppPoolName -physicalPath $IDMPortalServices -type Application
	$iisApp = New-Item 'IIS:\Sites\Default Web Site\idmapp' -ApplicationPool $iisAppPoolName -physicalPath $idmapp -type Application
	
	set-webconfiguration //system.webServer/security/authentication/anonymousAuthentication  machine/webroot/apphost -metadata overrideMode -value Allow
	Add-WebConfiguration �filter /system.webServer/security/authentication/anonymousAuthentication �IIS:\Sites\Default Web Site� 
	Set-WebConfigurationProperty -Filter /system.webServer/security/authentication/AnonymousAuthentication -location 'IIS:\Sites\Default Web Site\IDMPortalServices' -name username -value ""
	Set-WebConfigurationProperty -Filter /system.webServer/security/authentication/AnonymousAuthentication -location 'IIS:\Sites\Default Web Site\idmapp' -name username -value ""
	
	Write-Host "Copying contents..."
	Copy-Item -Path "idmportalservices" -Destination $InetPubWWWRoot -Force -Recurse
	Copy-Item -Path "idmapp" -Destination $InetPubWWWRoot -Force -Recurse
	
	
	$webConfig = $IDMPortalServices + '\web.config'
	Write-Host "Updating the IDMPortalServices web.config - " + $webConfig
    $doc = (get-content $webconfig) -as [xml]
	
	$obj = $doc.configuration.appsettings.add | where {$_.key -eq 'mongodbconnectionstring'}
	$obj.value = 'mongodb://' + $mdb	

	$obj1 = $doc.configuration.appsettings.add | where {$_.key -eq 'EcoSystemBaseUri'}
	$obj1.value = 'http://' + $idm + "/"
	
	$obj2 = $doc.configuration.appsettings.add | where {$_.key -eq 'eiioneemswebreference'}
	$obj2.value = $ems 		
	
	$obj3 = $doc.configuration.appsettings.add | where {$_.key -eq 'OneEMSEIIUser'}
	$obj3.value = $user
	
	$obj4 = $doc.configuration.appsettings.add | where {$_.key -eq 'OneEMSEIIPass'}
	$obj4.value = $pass
	
	$doc.Save($webConfig)

	# --------------------------------------------------------------------
	# Resetting IIS
	# --------------------------------------------------------------------
	Write-Host "IIS Reset..."
	$Command = "IISRESET"
	Invoke-Expression -Command $Command
	
	