﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Philips.HealthCare.IDM.Portal.DataAccess;

namespace Philips.HealthCare.IDM.Portal.WebController {
  public class MongoReader {
    MongoDbHelper helper;
    public MongoReader() {
      helper = new MongoDbHelper();
    }

    public string GetAllSites() {
      return helper.GetAllSitesHB();
    }

  }
}