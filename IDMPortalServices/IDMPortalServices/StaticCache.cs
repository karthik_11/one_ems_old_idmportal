﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Philips.HealthCare.IDM.Portal.DataAccess;
using Philips.HealthCare.IDM.Portal.DataObjects;
using MongoDB.Bson;

namespace Philips.HealthCare.IDM.Portal.Services
{
    [System.ComponentModel.DataObject]
    public class StaticCache
    {
        public static void LoadStaticCache()
        {
            MongoDbHelper helper = new MongoDbHelper();
            HttpContext.Current.Application["sites"] = helper.GetAllSites();
            HttpContext.Current.Application["namespaces"] = helper.GetAllServiceNamespaces();
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static List<SiteInfo> GetSites()
        {
            return HttpContext.Current.Application["sites"] as List<SiteInfo>;
        }

        [DataObjectMethodAttribute(DataObjectMethodType.Select, true)]
        public static List<ServiceNamespace> GetAllNamespaces()
        {
            return HttpContext.Current.Application["namespaces"] as List<ServiceNamespace>;
        }
    }
}