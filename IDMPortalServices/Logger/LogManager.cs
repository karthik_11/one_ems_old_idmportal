﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace Philips.HealthCare.IDM.Portal.Logger
{
  /// <summary>
  /// Singleton class to write logs into MongoDb
  /// </summary>
    public sealed class LogManager
    {

      private const string LogDatabaseName = "LogDb";
      private const string LogCollectionName = "LogCollection2";
      private const int CollectionSize = 100 * 1024 * 1024;

      private static readonly LogManager logWriter = new LogManager();

      private LogManager() { }

      /// <summary>
      /// Gets the log writer.
      /// </summary>
      /// <value>
      /// The log writer.
      /// </value>
      public static LogManager LogWriter {
        get {
          return logWriter;
        }
      }

      /// <summary>
      /// Writes the log.
      /// </summary>
      /// <param name="message">The message.</param>
      public void WriteLog(LogMessage message){
        var client = GetConnection();
        if (client == null) { 
          //TODO: Write to a file
        }
        try {
          //if (!client.GetServer().DatabaseExists(LogDatabasename)) {}
          var mongoDb  = client.GetServer().GetDatabase(LogDatabaseName);
          if (!mongoDb.CollectionExists(LogCollectionName)) {
            //Making a capped collection of 100MB
            var options = CollectionOptions.SetCapped(true).SetMaxSize(CollectionSize);
            mongoDb.CreateCollection(LogCollectionName, options);
          }
          var logCollection = mongoDb.GetCollection(LogCollectionName);
          logCollection.Insert(message);
        } catch (Exception ex) {
          //TODO: handle
        }
      }

      private static MongoClient GetConnection() {
        try {
          var connectionString = ConfigurationManager.AppSettings["MongoDbConnectionString"];
          return new MongoClient(connectionString);
        } catch (ConfigurationErrorsException ex) {          
          //Logger.Error(string.Format("Error while trying to read the MongoDBConnectionString from the config file", ex));
          return null;
        }
      }
    }
}
