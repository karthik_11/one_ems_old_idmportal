﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace Philips.HealthCare.IDM.Portal.Logger
{
    public class LogMessage
    {
        public LogMessage()
        {
            Type = "INFO";
            Severity = "NORMAL";
            Timestamp = DateTime.UtcNow;
        }

        public ObjectId Id { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Severity { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
