using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace Philips.HealthCare.IDM.Portal.OneEMS_ServiceInterfaceTest
{
    /// <summary>
    /// Class to connect to OneEMS and get sessionID
    /// </summary>
    public class OneEMSLogin
    {
        private OneEMSEnterprise.SforceService binding;
        private OneEMSEnterprise.LoginResult lr;

        public void login(string userName, string UserPassword)
        {
            try
            {
                binding = new OneEMSEnterprise.SforceService();
                lr = binding.login(userName, UserPassword);
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Login Exception: " + e.Message);
                throw new Exception("Login to SalesForce failed.");
            }
        }

        public string getSessionID { get { return lr.sessionId; } }
        public string getSessionURL { get { return lr.serverUrl; } }

        public void logout()
        {
            try
            {
                binding.logout();
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Logout Exception: " + e.Message);
            }
        }
    }
}