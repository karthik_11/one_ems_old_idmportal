﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Philips.HealthCare.IDM.Portal.Utilities {
    public static class DateTimeHelper {
        public static string CalculateDurationString(DateTime dateTime) {
            TimeSpan spanForDays = DateTime.Now - dateTime;
            return spanForDays.Duration().ToString(@"dd\d\ hh\h\ mm\m");
        }
    }
}
